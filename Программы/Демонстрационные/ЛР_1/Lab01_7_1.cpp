#include "../../sdt.h"

double solve_linear_equation(double k, double b)
{
    return -b / k;
}

int main()
{
    double k, b;
    cout << "Enter K and B for the K*x + B = 0 equation: ";
    cin  >> k >> b;

    if (k == 0)
    {
        cout << "Coef. k must not be 0!\n";
    }
    else
    {
        cout << "X = " << solve_linear_equation(k, b) << ".\n";
    }

    keep_window_open();
}
