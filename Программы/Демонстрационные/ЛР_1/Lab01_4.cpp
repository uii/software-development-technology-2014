#include "../../sdt.h"

int main()
{
    vector<double> x, y;

    unsigned int sample_size;
    cout << "Enter sample size: ";
    cin  >> sample_size;

    x.resize(sample_size);
    y.resize(sample_size);

    cout << "Enter elements of X, separated by spaces: ";
    for (unsigned int i = 0; i < sample_size; ++i)
    {
        cin >> x[i];
    }
    cout << "Enter elements of Y, separated by spaces: ";
    for (unsigned int i = 0; i < sample_size; ++i)
    {
        cin >> y[i];
    }

    double x_mean = 0;
    for (double item : x)
    {
        x_mean += item;
    }
    x_mean /= sample_size;

    double y_mean = 0;
    for (double item : y)
    {
        y_mean += item;
    }
    y_mean /= sample_size;

    double covariance = 0;
    double x_variance = 0;
    double y_variance = 0;
    for (unsigned int i = 0; i < sample_size; ++i)
    {
        double const dx = x[i] - x_mean;
        double const dy = y[i] - y_mean;
        x_variance += dx * dx;
        y_variance += dy * dy;
        covariance += dx * dy;
    }
    x_variance /= sample_size - 1;
    y_variance /= sample_size - 1;
    covariance /= sample_size - 1;

    double const correlation = covariance / sqrt(x_variance * y_variance);

    cout << "Correlation: " << correlation << "\n";

    keep_window_open();
}
