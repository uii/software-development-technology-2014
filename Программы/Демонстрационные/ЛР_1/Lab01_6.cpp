#include "../../sdt.h"

int main()
{
    unsigned int constexpr current_year = 2014;
    unsigned int constexpr mpei_foundation_year = 1935;
    unsigned int constexpr years_of_study = 6;

    char literal;
    int group_and_year;
    cout << "Enter a group number (e.g. A0209): ";
    cin  >> literal >> group_and_year;
    unsigned int const year = group_and_year % 100;

    // Opeartor ?: was not presented at the lecture (unfortunately).
    unsigned int admission_year;
    if (year + 2000 < current_year ||
        year + 1900 < mpei_foundation_year)
    {
        admission_year = 2000 + year;
    }
    else
    {
        admission_year = 1900 + year;
    }
    unsigned int const graduation_year = admission_year + years_of_study;

    cout << "Admission year (guessed): " << admission_year << ", "
         << "graduation year: " << graduation_year << ".\n";

    keep_window_open();
}
