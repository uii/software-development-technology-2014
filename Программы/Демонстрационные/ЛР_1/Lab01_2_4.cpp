#include "../../sdt.h"

int main()
{
    double constexpr absolute_zero_kelvin     = 0;
    double constexpr absolute_zero_celsius    = -273.15;
    double constexpr absolute_zero_fahrenheit = absolute_zero_celsius * 9. / 5 + 32;
    do
    {
        double temperature;
        char degree;
        cout << "Enter temperature with degree mark (e.g. 10K, 10C, or 10F): ";
        if (cin >> temperature >> degree)
        {
            double celsius, fahrenheit, kelvin;
            switch (degree)
            {
            case 'C':
                if (temperature < absolute_zero_celsius)
                {
                    cout << "Error, too cold (must be above " << absolute_zero_celsius << "C)!\n";
                    continue;
                }
                celsius    = temperature;
                fahrenheit = temperature * 9. / 5  + 32;
                kelvin     = temperature - absolute_zero_celsius;
                break;
            case 'F':
                if (temperature < absolute_zero_fahrenheit)
                {
                    cout << "Error, too cold (must be above " << absolute_zero_fahrenheit << "F)!\n";
                    continue;
                }
                celsius    = (temperature - 32) * 5. / 9;
                fahrenheit = temperature;
                kelvin     = celsius - absolute_zero_celsius;
                break;
            case 'K':
                if (temperature < absolute_zero_kelvin)
                {
                    cout << "Error, too cold (must be above " << absolute_zero_kelvin << "K)!\n";
                    continue;
                }
                celsius    = temperature + absolute_zero_celsius;
                fahrenheit = celsius * 9. / 5  + 32;
                kelvin     = temperature;
                break;
            default:
                cout << "Error, unknown degree mark! Must be C, F, or K.\n";
                continue;
            }
            cout << "This is " << celsius << "C, " << fahrenheit << "F, or " << kelvin << "K.\n";
        }
    }
    while (cin);

    cout << "Press ENTER twice or just close this window to exit.";
    keep_window_open();
}
