#include "../../sdt.h"

void input_vector(vector<double>& data)
{
    for (unsigned int i = 0; i < data.size(); ++i)
    {
        cin >> data[i];
    }
}

int main()
{
    unsigned int size;
    cout << "Enter vector size: ";
    cin  >> size;

    vector<double> data(size);
    cout << "Enter vector elements, separated by spaces: ";
    input_vector(data);

    cout << "You entered: ";
    for (double item : data)
    {
        cout << item << ' ';
    }
    cout << '\n';

    keep_window_open();
}
