#include "../../sdt.h"

int main()
{
    const vector<string> numbers
    {
        "zero",
        "one",      "two",       "three",    "four",     "five",
        "six",      "seven",     "eight",    "nine",     "ten",
        "eleven",   "twelve",    "thirteen", "fourteen", "fifteen",
        "sizeteen", "seventeen", "eighteen", "nineteen", "twenty"
    };

    int number = 1;

    cout << "Enter a textual number (e.g. 'minus seven'): ";
    string input;
    cin >> input;
    if (input == "minus")
    {
        number = -1;
        cin >> input;
    }

    bool found = false;
    for (unsigned int i = 0; i < numbers.size(); ++i)
    {
        if (numbers[i] == input)
        {
            found = true;
            number *= i;
            break;
        }
    }

    if (found)
    {
        cout << "Parsed: " << number << ".\n";
    }
    else
    {
        cout << "Unable to parse a number.\n";
    }

    keep_window_open();
}
