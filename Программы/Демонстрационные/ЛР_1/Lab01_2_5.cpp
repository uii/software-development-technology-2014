#include "../../sdt.h"

int main()
{
    double constexpr absolute_zero_kelvin     = 0;
    double constexpr absolute_zero_celsius    = -273.15;
    double constexpr absolute_zero_fahrenheit = absolute_zero_celsius * 9. / 5 + 32;

    vector<double> celsium, fahrenheits, kelvins;

    do
    {
        double temperature;
        char degree;
        cout << "Enter temperature with degree mark (e.g. 10K, 10C, or 10F): ";
        if (cin >> temperature >> degree)
        {
            double celsius, fahrenheit, kelvin;
            switch (degree)
            {
            case 'C':
                if (temperature < absolute_zero_celsius)
                {
                    cout << "Error, too cold (must be above " << absolute_zero_celsius << "C)!\n";
                    continue;
                }
                celsius    = temperature;
                fahrenheit = temperature * 9. / 5  + 32;
                kelvin     = temperature - absolute_zero_celsius;
                break;
            case 'F':
                if (temperature < absolute_zero_fahrenheit)
                {
                    cout << "Error, too cold (must be above " << absolute_zero_fahrenheit << "F)!\n";
                    continue;
                }
                celsius    = (temperature - 32) * 5. / 9;
                fahrenheit = temperature;
                kelvin     = celsius - absolute_zero_celsius;
                break;
            case 'K':
                if (temperature < absolute_zero_kelvin)
                {
                    cout << "Error, too cold (must be above " << absolute_zero_kelvin << "K)!\n";
                    continue;
                }
                celsius    = temperature + absolute_zero_celsius;
                fahrenheit = celsius * 9. / 5  + 32;
                kelvin     = temperature;
                break;
            default:
                cout << "Error, unknown degree mark! Must be C, F, or K.\n";
                continue;
            }
            celsium.push_back(celsius);
            fahrenheits.push_back(fahrenheit);
            kelvins.push_back(kelvin);
        }
    }
    while (cin);

    cout << "\nCelsius\tFahrenheit\tKelvin\n";
    for (unsigned int i = 0; i < celsium.size(); ++i)
    {
        cout << celsium[i] << "\t" << fahrenheits[i] << "\t\t" << kelvins[i] << "\n";
    }

    cout << "Press ENTER twice or just close this window to exit.";
    keep_window_open();
}
