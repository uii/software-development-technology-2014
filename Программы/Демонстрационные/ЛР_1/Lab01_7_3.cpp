#include "../../sdt.h"

vector<double> add(vector<double> a, vector<double> b)
{
    unsigned int const size = a.size();
    vector<double> result(size);
    for (unsigned int i = 0; i < size; ++i)
    {
        result[i] = a[i] + b[i];
    }
    return result;
}

// From previous task.
void input_vector(vector<double>& data)
{
    for (unsigned int i = 0; i < data.size(); ++i)
    {
        cin >> data[i];
    }
}

int main()
{
    unsigned int size;
    cout << "Enter vector size: ";
    cin  >> size;

    vector<double> a(size), b(size);
    cout << "Enter elements of vector A, separated by spaces: ";
    input_vector(a);
    cout << "Enter elements of vector B, separated by spaces: ";
    input_vector(b);

    vector<double> sum = add(a, b);

    cout << "The sum is: ";
    for (double item : sum)
    {
        cout << item << ' ';
    }
    cout << '\n';

    keep_window_open();
}

