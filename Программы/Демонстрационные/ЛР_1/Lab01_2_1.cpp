#include "../../sdt.h"

int main()
{
    double celsius;
    cout << "Enter temperature (degrees by Celsius): ";
    cin >> celsius;
    cout << "This is Fahrenheit " << (celsius * 9.0 / 5) + 32 << " or "
         << "Kelvin "             << (celsius + 273.15)       << ".\n";

    keep_window_open();
}
