#include "../../sdt.h"

int main()
{
    double const absolute_zero = -273.15;

    double celsius;
    cout << "Enter temperature (degrees by Celsius): ";
    cin >> celsius;
    if (celsius >= absolute_zero)
    {
        cout << "This is Fahrenheit " << (celsius * 9.0 / 5) + 32  << " or "
             << "Kelvin "             << (celsius - absolute_zero) << ".\n";
    }
    else
    {
        cout << "Error, temperature below absolute zero (Celsius " << absolute_zero << ")!\n";
    }

    keep_window_open();
}
