#include "../../sdt.h"

int main()
{
    unsigned int N;
    cout << "Enter N: ";
    cin >> N;
    for (unsigned int i = 1; i <= N; ++i)
    {
        for (unsigned int j = 1; j <= N; ++j)
        {
            cout << i*j << '\t';
        }
        cout << '\n';
    }

    keep_window_open();
}
