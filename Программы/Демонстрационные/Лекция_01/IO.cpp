#include "../../sdt.h"

int main()
{
    cout << "Enter your name and age: ";
    string name;
    unsigned int age;
    cin >> name >> age;
    const unsigned int next = age + 1;
    cout << "Hello, " << name << ", next year "
         << "you will be " << next << " years old.\n";
}
