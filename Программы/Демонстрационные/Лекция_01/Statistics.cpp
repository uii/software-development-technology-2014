#include "../../sdt.h"

int main()
{
    vector<double> xs;

    int n;
    cout << "Enter sample size: ";
    cin >> n;

    cout << "Enter " << n << " sample values: ";
    xs.resize(n);
    for (int i = 0; i < n; ++i) {
        cin >> xs[i];
    }

    double mean = 0;
    for (double x : xs) {
        mean += x;
    }
    mean /= xs.size();
    cout << "Mean is " << mean << ".\n";

    double variance = 0;
    if (n > 1) {
        for (double x : xs) {
            double d = x - mean;
            variance += d*d;
        }
        variance /= n - 1;
    }
    cout << "Variance is " << variance << ".\n";
}
